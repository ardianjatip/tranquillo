from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, RDF

def getGraph():
	g = Graph()
	g.parse("player.ttl", format="ttl")
	return g

def getGraph2():
	g = Graph()

	sparql1 = SPARQLWrapper("http://query.wikidata.org/sparql")
	query1 = """
		PREFIX fifa: <https://sofifa.com/player/>
		PREFIX db: <http://dbpedia.org/resource/>
		PREFIX dbo: <http://dbpedia.org/ontology/> 
		PREFIX wdt:	<http://www.wikidata.org/prop/direct/>
		PREFIX wd: <http://www.wikidata.org/entity/>

		CONSTRUCT 
		{
			?club rdfs:label ?clubLabel.
			?club wdt:P118  ?league.
            ?league rdfs:label ?leagueLabel.
            ?league wdt:P17 ?countryLabel.
		}
		WHERE
		{
            ?league wdt:P17 ?country.
			?club wdt:P31   wd:Q476028.
			?club wdt:P118  ?league.
            ?club rdfs:label ?clubLabel filter (lang(?clubLabel)="en").
            ?country rdfs:label ?countryLabel filter (lang(?countryLabel)="en").
            ?league rdfs:label ?leagueLabel filter (lang(?leagueLabel)="en").
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
		}
	"""
	sparql1.setQuery(query1)
	sparql1.setReturnFormat(RDF)
	results1 = sparql1.query()
	triples1 = results1.convert()
	g += triples1

	return g